#ifndef __Function_h__
#define __Function_h__

template <class T>
class Function0 {
public:
	virtual T call() = 0;
};


template <class T, class A>
class Function1 {
public:
	virtual T call(const A& a) = 0;
};


template <class T, class A, class B>
class Function2 {
public:
	virtual T call(const A& arg1, const B& arg2) = 0;
};

#endif // __Function_h__
