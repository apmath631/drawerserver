#include "Drawer.h"

Drawer::Drawer() {
  #ifdef PINS_CONNECTED
  for (int i = 0; i != PINS_STEPPER_COUNT; ++i) {
    pinMode(PINS_STEPPER_L[i], OUTPUT);
    digitalWrite(PINS_STEPPER_L[i], LOW);
    pinMode(PINS_STEPPER_R[i], OUTPUT);
    digitalWrite(PINS_STEPPER_R[i], LOW);
  }
  _pen_servo.attach(PIN_SERVO);
  #endif
}

Drawer::~Drawer() {}


void Drawer::setDoneCallback(DoneCallback* doneCallback) {
  _done_callback = doneCallback;
}

void Drawer::moveForward(float distance) {
  #ifdef LOG
  Serial.print("Forward: "); 
  Serial.println(distance);
  #endif
  performMove(distance, PINS_STEPPER_L, PINS_STEPPER_R, MASK_REV, MASK_FWD);
}

void Drawer::moveBackward(float distance) {
  #ifdef LOG
  Serial.print("Backward: "); 
  Serial.println(distance);
  #endif
  performMove(distance, PINS_STEPPER_L, PINS_STEPPER_R, MASK_FWD, MASK_REV);
}

void Drawer::turnLeft(float degrees) {
  #ifdef LOG
  Serial.print("Left: "); 
  Serial.println(degrees);
  #endif
  float rotation = degrees / MAX_DEGREES;
  float distance = WHEEL_BASE_MM * PI * rotation;
  performMove(distance, PINS_STEPPER_R, PINS_STEPPER_L, MASK_FWD, MASK_FWD);
}

void Drawer::turnRight(float degrees) {
  #ifdef LOG
  Serial.print("Right: "); 
  Serial.println(degrees);
  #endif
  float rotation = degrees / MAX_DEGREES;
  float distance = WHEEL_BASE_MM * PI * rotation;
  performMove(distance, PINS_STEPPER_R, PINS_STEPPER_L, MASK_REV, MASK_REV);
}

void Drawer::penUp() {
  #ifdef LOG
  Serial.println("Pen up");
  #endif
  performChangeServoAngle(PEN_ANGLE_UP);
}

void Drawer::penDown() {
  #ifdef LOG
  Serial.println("Pen down");
  #endif
  performChangeServoAngle(PEN_ANGLE_DOWN);
}

/* --- Private methods --- */
inline int Drawer::distanceToSteps(float distance) {
  return distance * STEPS_REV / (WHEEL_DIAMETER_MM * PI);
}

inline double Drawer::radiansToDegrees(double radians) {
  return radians * 57.2958;
}

void Drawer::performMove(
  float distance,
  const int firstStepperPins[], 
  const int secondStepperPins[], 
  const int firstMask[][MASK_SIZE], 
  const int secondMask[][MASK_SIZE]
) {
  if (distance < 0) {
    return;
  }
  
  int stepsCount = distanceToSteps(distance);
  #ifdef PINS_CONNECTED
  for (int step = 0; step != stepsCount; ++step) {
    for (int mask = 0; mask != MASK_SIZE; ++mask) {
      for (int pin = 0; pin != MASK_SIZE; ++pin) {
        digitalWrite(firstStepperPins[pin], firstMask[mask][pin]);
        digitalWrite(secondStepperPins[pin], secondMask[mask][pin]);
      }
      delay(STEP_DELAY_MS);
    }
  }
  #endif

  if (_done_callback != NULL) {
    _done_callback->call();
  }
}

void Drawer::performChangeServoAngle(int angle) {
  delay(PEN_DELAY_MS);
  _pen_servo.write(angle);
  delay(PEN_DELAY_MS);

  if (_done_callback != NULL) {
    _done_callback->call();
  }
}

void Drawer::done() {
  for (int mask = 0; mask != MASK_SIZE; ++mask) {
    for (int pin = 0; pin != MASK_SIZE; ++pin) {
      digitalWrite(PINS_STEPPER_R[pin], LOW);
      digitalWrite(PINS_STEPPER_L[pin], LOW);
    }
    delay(STEP_DELAY_MS);
  }
}
/* ---------------------- */
