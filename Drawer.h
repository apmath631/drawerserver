#ifndef __Drawer_h__
#define __Drawer_h__

#include "Function.h"
#include "Servo.h"

class DoneCallback : public Function0<void> {};

const int PIN_SERVO = D4;
const int PINS_STEPPER_COUNT = 4;

const int PINS_STEPPER_L[] = { D0, D2, D3, D1 };
const int PINS_STEPPER_R[] = { D8, D6, D5, D7 };

const int MASK_SIZE = 4;
const int MASK_FWD[MASK_SIZE][MASK_SIZE] = {
  { 1, 0, 1, 0 },
  { 0, 1, 1, 0 },
  { 0, 1, 0, 1 },
  { 1, 0, 0, 1 }
};
const int MASK_REV[MASK_SIZE][MASK_SIZE] = {
  { 1, 0, 0, 1 },
  { 0, 1, 0, 1 },
  { 0, 1, 1, 0 },
  { 1, 0, 1, 0 }
};

const int PEN_DELAY_MS = 250;
const int PEN_ANGLE_DOWN = 70;
const int PEN_ANGLE_UP = 140;

const int WHEEL_DIAMETER_MM = 60;
const int WHEEL_BASE_MM = 112;

const int STEPS_REV = 512;
const int STEP_DELAY_MS = 6;

const float MAX_DEGREES = 360.0;


class Drawer {
public:
  Drawer();
  ~Drawer();

  void setDoneCallback(DoneCallback* doneCallback);

  void moveForward(float distance);
  void moveBackward(float distance);
  void turnLeft(float degrees);
  void turnRight(float degrees);
  void penUp();
  void penDown();

private:
  inline int distanceToSteps(float distance);
  inline double radiansToDegrees(double radians);
  
  void performMove(float distance, 
    const int firstStepperPins[], const int secondStepperPins[], 
    const int firstMask[][MASK_SIZE], const int secondMask[][MASK_SIZE]
  );
  void performChangeServoAngle(int angle);
  void done();

  Servo _pen_servo;
  DoneCallback* _done_callback;
  
};

#endif // __Drawer_h__
