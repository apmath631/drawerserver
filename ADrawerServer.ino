#include "ArduinoServer.h"
#include "Drawer.h"

#define LOG
#define LOG_MSG
#define PINS_CONNECTED

const char* SSID = "212E";
const char* PASS = "Cneltyn_212";
const int PORT = 5000;

const String COMMAND_IN_FORWARD = "fwd";
const String COMMAND_IN_BACKWARD = "bwd";
const String COMMAND_IN_LEFT = "left";
const String COMMAND_IN_RIGHT = "right";
const String COMMAND_IN_PEN_UP = "penUp";
const String COMMAND_IN_PEN_DOWN = "penDown";

const String COMMAND_OUT_WRONG = "wrong";
const String COMMAND_OUT_DONE = "done";

const char CHAR_DELIMETER = ':';
const char CHAR_TERMINATE = ';';

ArduinoServer server;
Drawer drawer;


void parseCommand(const String& string);

class StringCallback: public ClientMessageCallback {
public:
  void call(const String& str) {
    parseCommand(str);
  }
} callback;

class DrawerCallback: public DoneCallback {
public:
  void call() {
    server.sendMessageToClient(COMMAND_OUT_DONE);
  }
} doneCallback;


void parseCommand(const String& string) {
  int delimeterIndex = string.indexOf(CHAR_DELIMETER);
  int terminateIndex = string.indexOf(CHAR_TERMINATE);
  if (terminateIndex < 0 || terminateIndex <= delimeterIndex) {
    #ifdef LOG
    Serial.println("String has wrong format: " + string);
    #endif
    server.sendMessageToClient(COMMAND_OUT_WRONG);
    return;
  }

  String command = "";
  float value = 0.0f;
  if (delimeterIndex < 0) {
    command = string.substring(0, terminateIndex);
  } else {
    command = string.substring(0, delimeterIndex);
    String stringValue = string.substring(delimeterIndex+1, terminateIndex);
    value = stringValue.toFloat();
  }

  #ifdef LOG
  Serial.println("Command: " + command);
  Serial.print("Value: "); Serial.println(value);
  #endif

  if (command == COMMAND_IN_FORWARD)        drawer.moveForward(value);
  else if (command == COMMAND_IN_BACKWARD)  drawer.moveBackward(value);
  else if (command == COMMAND_IN_LEFT)      drawer.turnLeft(value);
  else if (command == COMMAND_IN_RIGHT)     drawer.turnRight(value);
  else if (command == COMMAND_IN_PEN_UP)    drawer.penUp();
  else if (command == COMMAND_IN_PEN_DOWN)  drawer.penDown();
  else {
    #ifdef LOG
    Serial.println("Wrong command: " + command);
    #endif
    server.sendMessageToClient(COMMAND_OUT_WRONG);
    return;
  }
  
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  drawer.setDoneCallback(&doneCallback);
  server.connectToWiFiNetwork(SSID, PASS);
  server.setClientMessageCallback(&callback);
  server.startServer(PORT);
}

void loop() {
  server.handleClient();
}
