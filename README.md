# DrawerServer
---
Сервер для https://bitbucket.org/apmath631/drawerclient
Работает на чипах ESP8266.

### Принцип работы
Происходит подключение к существующей WiFi сети и начинается ожидание подключения клиента. После подключения клиента сервер принимает команды от клиента и исполняет их.

### Код
- ADrawerServer.ino – основной файл. В нём указываются данные WiFi сети и порт, к которому должен подключиться клиент.
- ArduinoServer.h/ArduinoServer.ino – работа с WiFi модулем
- Drawer.h/Drawer.ino – работа с устройством рисования
