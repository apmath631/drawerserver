#include "ArduinoServer.h"

ArduinoServer::ArduinoServer() {
	#ifdef LOG
	Serial.begin(SERIAL_SPEED);
	#endif
}

ArduinoServer::~ArduinoServer() {
	if (_server != NULL) delete _server;
}


void ArduinoServer::connectToWiFiNetwork(const char* ssid, const char* password) {
	WiFi.begin(ssid, password);
  #ifdef LOG
  Serial.println();
  Serial.print("Connecting.");
  #endif

  while (WiFi.status() != WL_CONNECTED) {
    delay(WIFI_CONNECTION_DELAY);
    #ifdef LOG
    Serial.print(".");
    #endif
  }
  
  #ifdef LOG
  Serial.println();
  Serial.print("Connected to "); Serial.println(SSID);
  Serial.print("Address: "); Serial.print(WiFi.localIP());
  Serial.print(":"); Serial.println(PORT);
  Serial.println();
  #endif
}

void ArduinoServer::startServer(int port) {
	if (_server == nullptr) {
		_server = new WiFiServer(port);
	}

	_server->begin();
	#ifdef LOG
	Serial.println("Server started");
	#endif
}

void ArduinoServer::handleClient() {
  if (_server->hasClient()) {
    if (!_client || !_client.connected()) {
      _client = _server->available();

      // Timeout to read string
      _client.setTimeout(CLIENT_READ_TIMEOUT_MS);
      #ifdef LOG
      Serial.println("New client");
      #endif
    }
  }

  // Receive message
  if (_client && _client.connected() && _client.available()) {
    String line = _client.readString();
    #ifdef LOG_MSG
    Serial.println("[Client]: " + line);
    #endif
    
    if (_callback != NULL) {
      _callback->call(line);
    }
  }

  // Send message to client from Serial
//  if (Serial.available()) {
//    size_t msg_size = Serial.available();
//    uint8_t msg_buffer[msg_size];
//    Serial.readBytes(msg_buffer, msg_size);
//    _client.write(msg_buffer, msg_size);
//
//    #ifdef LOG_MSG
//    if (msg_size > 0) {
//      Serial.write("[Server]: ");
//      for (int i = 0; i != msg_size; ++i) {
//        Serial.write(msg_buffer[i]);
//      }
//    }
//    #endif
//  }
}

void ArduinoServer::setClientMessageCallback(ClientMessageCallback* callback) {
  _callback = callback;
}

void ArduinoServer::sendMessageToClient(const String& message) {
  if (_client && _client.connected()) {
    _client.println(message);
  }
}

