#ifndef __ArduinoServer_h__
#define __ArduinoServer_h__

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include "Function.h"

class ClientMessageCallback : public Function1<void, String> {};

const int SERIAL_SPEED = 115200;
const int CLIENT_READ_TIMEOUT_MS = 100;
const int WIFI_CONNECTION_DELAY = 1000;

class ArduinoServer {
public:
	ArduinoServer();
	~ArduinoServer();

	void connectToWiFiNetwork(const char* ssid, const char* password);
  void startServer(int port);
  void setClientMessageCallback(ClientMessageCallback* callback);
  
  void handleClient();
  
  void sendMessageToClient(const String& message);

private:
	WiFiServer* _server;
  WiFiClient _client;
  ClientMessageCallback* _callback;
  
};

#endif // __ArduinoServer_h__
